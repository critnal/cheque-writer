﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChequeWriter;

namespace ChequeWriterTest
{
    [TestClass]
    public class ChequeWriterTests
    {
        Cheque cheque = new Cheque();

        [TestMethod]
        public void TestInitialValues()
        {
            Assert.AreNotEqual("Invalid amount", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestInvalidCharacters()
        {
            string invalidCharacters = " abcdefghijklmnopqrstuvwxyz-_=+!@#$%^&*();:[]{}";
            for (int i = 0; i < invalidCharacters.Length; i++)
            {
                cheque.EnterCashAmountInNumbers(invalidCharacters[i].ToString());
                Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
            }
        }

        [TestMethod]
        public void TestDecimalWithNoCents()
        {
            cheque.EnterCashAmountInNumbers("2.");
            Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestTooManyDecimals()
        {
            cheque.EnterCashAmountInNumbers("2..22");
            Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("2.2.2");
            Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestZeroDollars()
        {
            cheque.EnterCashAmountInNumbers("0");
            Assert.AreEqual("ZERO DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestOnes()
        {
            cheque.EnterCashAmountInNumbers("1");
            Assert.AreEqual("ONE DOLLAR", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("2");
            Assert.AreEqual("TWO DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("3");
            Assert.AreEqual("THREE DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("4");
            Assert.AreEqual("FOUR DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("5");
            Assert.AreEqual("FIVE DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("6");
            Assert.AreEqual("SIX DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("7");
            Assert.AreEqual("SEVEN DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("8");
            Assert.AreEqual("EIGHT DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("9");
            Assert.AreEqual("NINE DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestTeens()
        {
            cheque.EnterCashAmountInNumbers("11");
            Assert.AreEqual("ELEVEN DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("12");
            Assert.AreEqual("TWELVE DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("13");
            Assert.AreEqual("THIRTEEN DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("14");
            Assert.AreEqual("FOURTEEN DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("15");
            Assert.AreEqual("FIFTEEN DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("16");
            Assert.AreEqual("SIXTEEN DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("17");
            Assert.AreEqual("SEVENTEEN DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("18");
            Assert.AreEqual("EIGHTEEN DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("19");
            Assert.AreEqual("NINETEEN DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestTens()
        {
            cheque.EnterCashAmountInNumbers("10");
            Assert.AreEqual("TEN DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("20");
            Assert.AreEqual("TWENTY DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("30");
            Assert.AreEqual("THIRTY DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("40");
            Assert.AreEqual("FORTY DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("50");
            Assert.AreEqual("FIFTY DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("60");
            Assert.AreEqual("SIXTY DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("70");
            Assert.AreEqual("SEVENTY DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("80");
            Assert.AreEqual("EIGHTY DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("90");
            Assert.AreEqual("NINETY DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestTensWithOnes()
        {
            cheque.EnterCashAmountInNumbers("21");
            Assert.AreEqual("TWENTY-ONE DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("32");
            Assert.AreEqual("THIRTY-TWO DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("43");
            Assert.AreEqual("FORTY-THREE DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("54");
            Assert.AreEqual("FIFTY-FOUR DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("65");
            Assert.AreEqual("SIXTY-FIVE DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("76");
            Assert.AreEqual("SEVENTY-SIX DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("87");
            Assert.AreEqual("EIGHTY-SEVEN DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("98");
            Assert.AreEqual("NINETY-EIGHT DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("99");
            Assert.AreEqual("NINETY-NINE DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestHundreds()
        {
            cheque.EnterCashAmountInNumbers("100");
            Assert.AreEqual("ONE HUNDRED DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("200");
            Assert.AreEqual("TWO HUNDRED DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("300");
            Assert.AreEqual("THREE HUNDRED DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("400");
            Assert.AreEqual("FOUR HUNDRED DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("500");
            Assert.AreEqual("FIVE HUNDRED DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("600");
            Assert.AreEqual("SIX HUNDRED DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("700");
            Assert.AreEqual("SEVEN HUNDRED DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("800");
            Assert.AreEqual("EIGHT HUNDRED DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("900");
            Assert.AreEqual("NINE HUNDRED DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestHundredsWithOnes()
        {
            cheque.EnterCashAmountInNumbers("101");
            Assert.AreEqual("ONE HUNDRED AND ONE DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestHundredsWithTens()
        {
            cheque.EnterCashAmountInNumbers("110");
            Assert.AreEqual("ONE HUNDRED AND TEN DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestHundredsWithTeens()
        {
            cheque.EnterCashAmountInNumbers("111");
            Assert.AreEqual("ONE HUNDRED AND ELEVEN DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestThousands()
        {
            cheque.EnterCashAmountInNumbers("1000");
            Assert.AreEqual("ONE THOUSAND DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestThousandsWithOnes()
        {
            cheque.EnterCashAmountInNumbers("1001");
            Assert.AreEqual("ONE THOUSAND AND ONE DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestThousandsWithTens()
        {
            cheque.EnterCashAmountInNumbers("1010");
            Assert.AreEqual("ONE THOUSAND AND TEN DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestThousandsWithTeens()
        {
            cheque.EnterCashAmountInNumbers("1011");
            Assert.AreEqual("ONE THOUSAND AND ELEVEN DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestThousandsWithHundreds()
        {
            cheque.EnterCashAmountInNumbers("1100");
            Assert.AreEqual("ONE THOUSAND ONE HUNDRED DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestThousandsWithHundredsAndOnes()
        {
            cheque.EnterCashAmountInNumbers("1101");
            Assert.AreEqual("ONE THOUSAND ONE HUNDRED AND ONE DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestThousandsWithHundredsAndTens()
        {
            cheque.EnterCashAmountInNumbers("1110");
            Assert.AreEqual("ONE THOUSAND ONE HUNDRED AND TEN DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestPowersOfThousands()
        {
            cheque.EnterCashAmountInNumbers("1000000");
            Assert.AreEqual("ONE MILLION DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("1000000000");
            Assert.AreEqual("ONE BILLION DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("1000000000000");
            Assert.AreEqual("ONE TRILLION DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("1000000000000000");
            Assert.AreEqual("ONE QUADRILLION DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestMultiplePowersOfThousands()
        {
            cheque.EnterCashAmountInNumbers("1001001001001000");
            Assert.AreEqual("ONE QUADRILLION ONE TRILLION ONE BILLION ONE MILLION ONE THOUSAND DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestCommaSeparation()
        {
            cheque.EnterCashAmountInNumbers("1,000");
            Assert.AreEqual("ONE THOUSAND DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("10,000");
            Assert.AreEqual("TEN THOUSAND DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("100,000");
            Assert.AreEqual("ONE HUNDRED THOUSAND DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("1,000,000");
            Assert.AreEqual("ONE MILLION DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("10,000,000");
            Assert.AreEqual("TEN MILLION DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("100,000,000");
            Assert.AreEqual("ONE HUNDRED MILLION DOLLARS", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("1,000,000,000");
            Assert.AreEqual("ONE BILLION DOLLARS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestInvalidCommaSeparation()
        {
            cheque.EnterCashAmountInNumbers("10,00");
            Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("100,0");
            Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("1000,");
            Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers(",1000");
            Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
            cheque.EnterCashAmountInNumbers("1,,000");
            Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestCentsWithTooManyUnits()
        {
            cheque.EnterCashAmountInNumbers(".001");
            Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestCentsWithNoDollars()
        {
            cheque.EnterCashAmountInNumbers(".01");
            Assert.AreEqual("Invalid amount", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestCentsWithZeroDollarsAndZeroCents()
        {
            cheque.EnterCashAmountInNumbers("0.00");
            Assert.AreEqual("ZERO DOLLARS AND ZERO CENTS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestCentsWithZeroDollars()
        {
            cheque.EnterCashAmountInNumbers("0.02");
            Assert.AreEqual("ZERO DOLLARS AND TWO CENTS", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestOneCent()
        {
            cheque.EnterCashAmountInNumbers("0.01");
            Assert.AreEqual("ZERO DOLLARS AND ONE CENT", cheque.CashAmountInWords);
        }

        [TestMethod]
        public void TestOneTwoThreeDecimalFourFive()
        {
            cheque.EnterCashAmountInNumbers("123.45");
            Assert.AreEqual("ONE HUNDRED AND TWENTY-THREE DOLLARS AND FORTY-FIVE CENTS", cheque.CashAmountInWords);
        }
        
    }
}
