﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChequeWriter
{
    public static class NumberToWordConverter
    {
        #region Class Variables and Constructors

        private static Dictionary<char, string> ones;
        private static Dictionary<string, string> teens;
        private static Dictionary<char, string> tens;
        private static Dictionary<int, string> powersOfOneThousand;

        static NumberToWordConverter()
        {
            ones = new Dictionary<char, string>()
            {
                { '0', "ZERO" },
                { '1', "ONE" },
                { '2', "TWO" },
                { '3', "THREE" },
                { '4', "FOUR" },
                { '5', "FIVE" },
                { '6', "SIX" },
                { '7', "SEVEN" },
                { '8', "EIGHT" },
                { '9', "NINE" }
            };
            teens = new Dictionary<string, string>()
            {
                { "10", "TEN" },
                { "11", "ELEVEN" },
                { "12", "TWELVE" },
                { "13", "THIRTEEN" },
                { "14", "FOURTEEN" },
                { "15", "FIFTEEN" },
                { "16", "SIXTEEN" },
                { "17", "SEVENTEEN" },
                { "18", "EIGHTEEN" },
                { "19", "NINETEEN" }
            };
            tens = new Dictionary<char, string>()
            {
                { '0', "" },
                { '2', "TWENTY" },
                { '3', "THIRTY" },
                { '4', "FORTY" },
                { '5', "FIFTY" },
                { '6', "SIXTY" },
                { '7', "SEVENTY" },
                { '8', "EIGHTY" },
                { '9', "NINETY" }
            };
            powersOfOneThousand = new Dictionary<int, string>()
            {
                { 0, "" },
                { 1, "THOUSAND" },
                { 2, "MILLION" },
                { 3, "BILLION" },
                { 4, "TRILLION" },
                { 5, "QUADRILLION" }
            };
        }

        #endregion

        #region High Level Processing

        public static string ConvertNumbersToWords(string amountInNumbers)
        {
            amountInNumbers = amountInNumbers.Replace(",", "");

            string dollarsInWords = getDollarsInWords(amountInNumbers);
            string centsInWords = "";
            if (doesContainCents(amountInNumbers))
            {
                centsInWords = getCentsInWords(amountInNumbers);
            }
            return dollarsInWords + centsInWords;
        }

        private static string getDollarsInWords(string amountInNumbers)
        {
            string dollarsInNumbers = amountInNumbers.Split('.')[0];
            if (isOnlyOneDollar(dollarsInNumbers))
            {
                return "ONE DOLLAR";
            }
            if (isZeroDollars(dollarsInNumbers))
            {
                return "ZERO DOLLARS";
            }
            string dollarsInWords = "";

            List<string> segments = getThousandDollarSegments(dollarsInNumbers);
            string segmentInWords = "";
            int powerOfOneThousand = 0;
            for (int i = segments.Count - 1; i >= 0; i--)
            {
                if (!isBlankSegment(segments[i]))
                {
                    bool isFirstSegment = (powerOfOneThousand == (segments.Count - 1));
                    bool isLastSegment = (powerOfOneThousand == 0);
                    segmentInWords = getSegmentInWords(segments[i], isFirstSegment, isLastSegment);
                    segmentInWords += getFromPowerOfOneThousand(powerOfOneThousand);
                    dollarsInWords = segmentInWords + dollarsInWords;
                }                
                powerOfOneThousand++;
            }
            return dollarsInWords + " DOLLARS";
        }

        private static bool isOnlyOneDollar(string dollarsInNumbers)
        {
            if (dollarsInNumbers.Length == 1 && dollarsInNumbers[0] == '1')
            {
                return true;
            }
            return false;
        }

        private static bool isZeroDollars(string dollarsInNumbers)
        {
            if (dollarsInNumbers.Length == 1 && dollarsInNumbers[0] == '0')
            {
                return true;
            }
            return false;
        }

        private static bool isBlankSegment(string segment)
        {
            foreach (char character in segment)
            {
                if (character != '0')
                {
                    return false;
                }
            }
            return true;
        }

        private static List<string> getThousandDollarSegments(string dollarsInNumbers)
        {
            List<string> thousandDollarSegments = new List<string>();
            string currentSegment = "";
            int count = 0;
            for (int i = dollarsInNumbers.Length - 1; i >= 0; i--)
            {
                if (count % 3 == 0)
                {
                    currentSegment = "";
                    thousandDollarSegments.Insert(0, currentSegment);
                }
                currentSegment = dollarsInNumbers[i] + currentSegment;
                thousandDollarSegments[0] = currentSegment;
                count++;
            }
            return thousandDollarSegments;
        }

        private static string getSegmentInWords(string segment, bool isFirstSegment, bool isLastSegment)
        {
            string segmentInWords = " ";
            if (isFirstSegment)
            {
                segmentInWords = "";
            }
            if (segment.Length >= 3)
            {
                if (segment[0] != '0')
                {
                    segmentInWords += getOnesInWords(segment[0]) + " HUNDRED";
                    if (segment[1] != '0' || segment[2] != '0')
                    {
                        segmentInWords += " AND ";
                    }
                }
                else if (isLastSegment)
                {
                    segmentInWords += "AND ";
                }
                if (segment[1] != '0' || segment[2] != '0')
                {
                    segmentInWords += getTensInWords(segment.Substring(1, 2));
                }                
            }
            else if (segment.Length >= 2)
            {
                segmentInWords += getTensInWords(segment);
            }
            else if (segment.Length >= 1)
            {
                segmentInWords += getOnesInWords(segment[0]);
            }
            return segmentInWords;
        }

        private static bool doesContainCents(string amountInNumbers)
        {
            return amountInNumbers.Contains(".");
        }

        private static string getCentsInWords(string amountInNumbers)
        {
            string centsInNumbers = amountInNumbers.Split('.')[1];
            if (centsInNumbers == "01")
            {
                return " AND ONE CENT";
            }
            if (centsInNumbers == "00")
            {
                return " AND ZERO CENTS";
            }
            return " AND " + getTensInWords(centsInNumbers) + " CENTS";
        }

        #endregion

        #region Low Level Conversion

        private static string getTensInWords(string tensInNumbers)
        {
            string tensInWords = "";
            if (tensInNumbers[0] == '1')
            {
                tensInWords += getFromTeens(tensInNumbers);
            }
            else
            {
                if (tensInNumbers[0] != '0')
                {
                    tensInWords += getFromTens(tensInNumbers[0]);
                    if (tensInNumbers[1] != '0')
                    {
                        tensInWords += "-";
                    }
                }
                if (tensInNumbers[1] != '0')
                {
                    tensInWords += getFromOnes(tensInNumbers[1]);
                }
            }
            return tensInWords;
        }

        private static string getOnesInWords(char hundredsAsANumber)
        {
            return getFromOnes(hundredsAsANumber);
        }

        private static string getFromOnes(char number)
        {
            return ones[number];
        }

        private static string getFromTeens(string number)
        {
            return teens[number];
        }

        private static string getFromTens(char number)
        {
            return tens[number];
        }

        private static string getFromPowerOfOneThousand(int power)
        {
            if (powersOfOneThousand.ContainsKey(power))
            {
                if (powersOfOneThousand[power] != "")
                {
                    return " " + powersOfOneThousand[power] + "";
                }
            }
            return "";
        }

        #endregion
    }
}
