﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ChequeWriter
{
    public class Cheque
    {
        public string Date { get; protected set; }
        public string Recipient { get; protected set; }
        public string CashAmountInNumbers { get; protected set; }
        public string CashAmountInWords { get; protected set; }

        public Cheque ()
        {
            Date = System.DateTime.Now.ToShortDateString();
            Recipient = "Some Company";
            CashAmountInNumbers = "0.00";
        }

        public virtual void EnterCashAmountInNumbers(string input)
        {
            CashAmountInNumbers = constrainCharacters(input);
            if (isValidCurrencyAmount(CashAmountInNumbers))
            {
                CashAmountInWords = NumberToWordConverter.ConvertNumbersToWords(CashAmountInNumbers);
            }
            else
            {
                CashAmountInWords = "Invalid amount";
            }
        }

        protected virtual string constrainCharacters(string amountInNumbers)
        {
            return Regex.Replace(amountInNumbers, @"^[^0-9.,]$", "");
        }

        protected virtual bool isValidCurrencyAmount(string amountInNumbers)
        {
            return Regex.IsMatch(amountInNumbers, @"^((([1-9][0-9]{0,2}),([0-9]{3},)*([0-9]{3}))|([1-9][0-9]*)|0)(\.[0-9]{2})?$");
        }
    }
}
