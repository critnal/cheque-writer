﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChequeWriter
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ChequeForm chequeForm = new ChequeForm();
            Presenter presenter = new Presenter(chequeForm, new Cheque());
            Application.Run(chequeForm);
        }
    }
}
