﻿namespace ChequeWriter
{
    partial class ChequeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cashAmountInNumbersTextBox = new System.Windows.Forms.TextBox();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.recipientTextBox = new System.Windows.Forms.TextBox();
            this.cashAmountInWordsTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cashAmountInNumbersTextBox
            // 
            this.cashAmountInNumbersTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cashAmountInNumbersTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cashAmountInNumbersTextBox.Location = new System.Drawing.Point(603, 149);
            this.cashAmountInNumbersTextBox.MaxLength = 21;
            this.cashAmountInNumbersTextBox.Name = "cashAmountInNumbersTextBox";
            this.cashAmountInNumbersTextBox.Size = new System.Drawing.Size(185, 32);
            this.cashAmountInNumbersTextBox.TabIndex = 0;
            // 
            // dateTextBox
            // 
            this.dateTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBox.Location = new System.Drawing.Point(620, 81);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(168, 32);
            this.dateTextBox.TabIndex = 1;
            // 
            // recipientTextBox
            // 
            this.recipientTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.recipientTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recipientTextBox.Location = new System.Drawing.Point(75, 115);
            this.recipientTextBox.Name = "recipientTextBox";
            this.recipientTextBox.Size = new System.Drawing.Size(492, 32);
            this.recipientTextBox.TabIndex = 2;
            // 
            // cashAmountInWordsTextBox
            // 
            this.cashAmountInWordsTextBox.AcceptsReturn = true;
            this.cashAmountInWordsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cashAmountInWordsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cashAmountInWordsTextBox.Location = new System.Drawing.Point(127, 171);
            this.cashAmountInWordsTextBox.Multiline = true;
            this.cashAmountInWordsTextBox.Name = "cashAmountInWordsTextBox";
            this.cashAmountInWordsTextBox.Size = new System.Drawing.Size(440, 150);
            this.cashAmountInWordsTextBox.TabIndex = 3;
            // 
            // ChequeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ChequeWriter.Properties.Resources.ChequeBackground;
            this.ClientSize = new System.Drawing.Size(800, 359);
            this.Controls.Add(this.cashAmountInWordsTextBox);
            this.Controls.Add(this.recipientTextBox);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.cashAmountInNumbersTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ChequeForm";
            this.Text = "Cheque Writer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox cashAmountInNumbersTextBox;
        private System.Windows.Forms.TextBox dateTextBox;
        private System.Windows.Forms.TextBox recipientTextBox;
        private System.Windows.Forms.TextBox cashAmountInWordsTextBox;
    }
}

