﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChequeWriter
{
    public partial class ChequeForm : Form
    {
        public ChequeForm()
        {
            InitializeComponent();
        }

        public TextBox GetTextBox(string textboxName)
        {
            return (TextBox)Controls[textboxName];
        }
    }
}
