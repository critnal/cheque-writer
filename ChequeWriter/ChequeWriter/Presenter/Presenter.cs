﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace ChequeWriter
{
    public class Presenter
    {
        protected ChequeForm chequeForm;
        protected Cheque cheque;

        protected TextBox dateTextBox;
        protected TextBox recipientTextBox;
        protected TextBox cashAmountInNumbersTextBox;
        protected TextBox cashAmountInWordsTextBox;
        protected Color defaultTextBoxBackColor;

        public Presenter(ChequeForm chequeForm, Cheque cheque)
        {
            this.chequeForm = chequeForm;
            dateTextBox = chequeForm.GetTextBox("dateTextBox");
            recipientTextBox = chequeForm.GetTextBox("recipientTextBox");
            cashAmountInNumbersTextBox = chequeForm.GetTextBox("cashAmountInNumbersTextBox");
            cashAmountInWordsTextBox = chequeForm.GetTextBox("cashAmountInWordsTextBox");
            cashAmountInNumbersTextBox.TextChanged += cashAmountInNumbersTextBox_TextChanged;

            this.cheque = cheque;
            dateTextBox.Text = cheque.Date;
            recipientTextBox.Text = cheque.Recipient;
            cashAmountInNumbersTextBox.Text = cheque.CashAmountInNumbers;
            cashAmountInWordsTextBox.Text = cheque.CashAmountInWords;
            defaultTextBoxBackColor = dateTextBox.BackColor;
        }

        protected virtual void cashAmountInNumbersTextBox_TextChanged(object sender, EventArgs e)
        {
            cheque.EnterCashAmountInNumbers(cashAmountInNumbersTextBox.Text);

            if (cheque.CashAmountInWords == "Invalid amount")
            {
                cashAmountInNumbersTextBox.BackColor = Color.Crimson;
            }
            else
            {
                cashAmountInNumbersTextBox.BackColor = defaultTextBoxBackColor;
            }
            cashAmountInWordsTextBox.Text = cheque.CashAmountInWords;
        }
    }
}
